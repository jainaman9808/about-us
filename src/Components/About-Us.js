import React from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader } from 'reactstrap';
import { Link } from 'react-router-dom';
import Faqs from './FAQ/faqs'
function About() {
    return(
        <div className="container">
            <div className="row back_colour">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem active>About Us</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12 ml-3 text-white">
                    <h3>About Us</h3>
                    <hr />
                </div>                
            </div>
            <div className="row row-content">
                <div className="col-12 col-md-6 ml-3">
                    <h2>Our History</h2>
                    <p>We are India's premium crowdfunding platform designed to help innovators/entrepreneurs to raise funds for potential innovations to disrupt the market. A growing startup founded by two engineers turned innovators who want to help out fellow future CEOs and startups.</p>
                    <p> FundingX has been derived from the idea of funding X in its literal sense. X can be any variable, in this case, any startup that deserves funding. We hope to make an India where startups are the Call of Duty</p>
                </div>
                <div className="col-12 col-md-5 mt-3">
                    <Card>
                        <CardHeader body inverse style={{ backgroundColor: '#333', borderColor: '#333' }} className="text-white">Facts At a Glance</CardHeader>
                        <CardBody className="back_clr">
                            <dl className="row p-1">
                                <dt className="col-6">Website</dt>
                                <dd className="col-6"><Link>http://www.fundingx.in</Link></dd>
                                <dt className="col-6">Industry</dt>
                                <dd className="col-6">Fundraising</dd>
                                <dt className="col-6">Company size</dt>
                                <dd className="col-6">2-10 employees</dd>
                                <dt className="col-6">Type</dt>
                                <dd className="col-6">Privately Held</dd>
                            </dl>
                        </CardBody>
                    </Card>
                </div>
                <div className="col-11 mt-4 ml-3">
                    <Card>
                        <CardBody className="bg-faded back_clr">
                            <blockquote className="blockquote">
                                <p className="mb-0">"Let our advance worrying become advance thinking and planning."</p>
                                <footer className="blockquote-footer"> ~ Winston Churchill..
                                </footer>
                            </blockquote>
                        </CardBody>
                    </Card>
                </div>
                <Faqs/>

                <div className="col-12 mt-4 d-flex justify-content-center">
                      <Link className="m-5">PRIVACY POLICY</Link>
                      <Link className="m-5">TERMS & CONDITION</Link>
                      <Link className="m-5">COMPAIGN OWNER AGREEMENT</Link>
                </div>
            </div>
            
        </div>
    );
}

export default About;