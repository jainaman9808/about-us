import React, { useState } from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import FaqData from './faqutil'
import './style.css'
console.log(FaqData);
const EachFaq = ({data}) => {
  const [isOpen, setIsOpen] = useState(false);
    console.log(data);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <p color="secondary hoverable" class = "faq-ques d-flex justify-content-between" onClick={toggle} >
          <span>{data.ques}</span>
          <span class="font-weight-bold">
          <i class="fas fa-chevron-down"></i>
        </span>
          </p>
      <Collapse  isOpen={isOpen}>
        <Card class="border-0">
          <CardBody>
            {data.ans}
          </CardBody>
        </Card>
      </Collapse>
    </div>
  );
}

const Faqs = () => {
    console.log("here");
    return (
            <div class="container">
                <h3 class="title-block mt-3">FAQ's</h3>

                {FaqData.map((i) => <EachFaq data= {i} /> ) }
            </div>
    );
}

export default Faqs;